# SPDX-License-Identifier: GPL-2.0-or-later


set(path_SRC
  path-object-set.cpp
  path-outline.cpp

  # -------
  # Headers
  path-outline.h
)

add_inkscape_source("${path_SRC}")
